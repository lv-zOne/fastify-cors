const fastify = require('fastify')({ logger: true });
const serverPort = 3500;
const path = require('path')
const staticAssetsPath = '/assets/'; //for all css, images etc assets to be served inside views/rendered html
const staticLocalFolder = path.join(__dirname, '/server/public'); //local folder mapped to the assets path

//CORS issue resolve - if you comment the block below, you can reproduce again the CORS issue
// fastify.register(require('fastify-cors'),{
//     origin: true,
//     allowedHeaders: ['Origin', 'X-Requested-With', 'Accept', 'Content-Type', 'Authorization'],
//     //methods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'] // you can also limit by specific methods, or allow all
// });

//EJS Template Renderer for Fastify
fastify.register(require('point-of-view'), {
    engine: {
        ejs: require('ejs')
    }
});

//Static files for Fastify - css, images, non-NodeJS scripts
fastify.register(require('fastify-static'), {
    root: staticLocalFolder,
    prefix: staticAssetsPath,
});

//API calls - also different based on method
//XHR - GET - extract payload from query parameters
fastify.get('/api/xhr', (req, reply) => {
    reply.send({ 
        serverResponseMethodUsed: 'GET',
        serverResponseDataSentVia: 'XMLHttpRequest / AJAX',
        serverResponseField: 'Value for Response Field',
        serverResponseOriginalPayloadSent: req.query,
    })
});

//XHR - POST - extract payload from request
fastify.post('/api/xhr', (req, reply) => {
    reply.send({ 
        serverResponseMethodUsed: 'POST',
        serverResponseDataSentVia: 'XMLHttpRequest / AJAX',
        serverResponseField: 'Value for Response Field',
        serverResponseOriginalPayloadSent: req.body,
    })
});

//Fetch - GET - extract payload from query parameters
fastify.get('/api/fetch', (req, reply) => {
    reply.send({ 
        serverResponseMethodUsed: 'GET',
        serverResponseDataSentVia: 'Fetch API',
        serverResponseField: 'Value for Response Field',
        serverResponseOriginalPayloadSent: req.query,
    })
});

//Fetch - POST - extract payload from request
fastify.post('/api/fetch', (req, reply) => {
    reply.send({ 
        serverResponseMethodUsed: 'POST',
        serverResponseDataSentVia: 'Fetch API',
        serverResponseField: 'Value for Response Field',
        serverResponseOriginalPayloadSent: JSON.parse(req.body),
    })
});

// Home root route
fastify.get('/', async (request, reply) => {
    reply.view(`./server/views/root.ejs`,{assetsPath: staticAssetsPath});
    return reply;
});

// Run the server!
const start = async () => {
    try {
        await fastify.listen(serverPort);
        fastify.log.info(`SERVER listening on ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
}

start();