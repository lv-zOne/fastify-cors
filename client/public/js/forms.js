//Plain JS no ES6+ (no const, let, arrow functions, spread operator, etc -
//not because it's easy, but because it's more difficult ;) (I'm so used to 
//ES6 code transpiled to JS these days, but I thought I'd try and do it all in Vanilla JS here)
//also as I'm writing this to demo code that can be run natively in all browsers 
//Note: I've added a Fetch API Polifyll in head too

//Return object with form field elements values for the given method
var apiObjectGen = function(solution, method) { //e.x. solution = 'XHR', method = 'POST'
    return {
        firstName: document.getElementById('firstname_' + solution.toLowerCase() + '_' + method.toLowerCase()).value,
        lastName: document.getElementById('lastname_' + solution.toLowerCase() + '_' + method.toLowerCase()).value,
        email: document.getElementById('email_' + solution.toLowerCase() + '_' + method.toLowerCase()).value,
        message: document.getElementById('message_' + solution.toLowerCase() + '_' + method.toLowerCase()).value,
        hidden: document.getElementById('hidden-field_' + solution.toLowerCase() + '_' + method.toLowerCase()).value,
    };
};

//On DOM Ready
document.addEventListener('DOMContentLoaded', function(domLoadedEvent) {
    //Get form references
    var xhrPostForm = document.getElementById('form_xhr_post');
    var xhrGetForm = document.getElementById('form_xhr_get');
    var fetchPostForm = document.getElementById('form_fetch_post');
    var fetchGetForm = document.getElementById('form_fetch_get');

    //Form submit listeners  
    //XMLHttpRequest - POST
    xhrPostForm.addEventListener('submit', function(e){
        //Get form data upon form submit
        var xhrPostData = apiObjectGen('XHR','POST');
        console.log('Attempting to AJAX (XMLHttpRequest) data to server using POST method, data:', xhrPostData);

        //Prevent browser's native form submit as we'll use own solution
        e.preventDefault();

        //Prepare new Request
        var oReq = new XMLHttpRequest();

        //Prepare listener for when the request is done
        oReq.addEventListener("readystatechange", function(o){
            if (this.readyState == 4) {
                console.log('Response from server:', JSON.parse(this.response));
            }
        });

        //Open Request
        oReq.open(
            xhrPostForm.getAttribute('method').toUpperCase(),
            xhrPostForm.getAttribute('action'),
            true);

        //Set Request header - content type json
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        //Send request payload
        oReq.send(JSON.stringify(xhrPostData));
    });

    //XMLHttpRequest - GET
    xhrGetForm.addEventListener('submit', function(e){
        var xhrGetData = apiObjectGen('XHR','GET');
        console.log('Attempting to AJAX (XMLHttpRequest) data to server using GET method, data:', xhrGetData);

        //Prevent browser's native form submit as we'll use own solution
        e.preventDefault();
        
        //Prepare new Request
        var oReq = new XMLHttpRequest();

        //Prepare listener for when the request is done
        oReq.addEventListener("readystatechange", function(o){
            if (this.readyState == 4) {
                console.log('Response from server:', JSON.parse(this.response));
            }
        });

        //Prepare JSON data as GET parameters
        var urlParams = Object.keys(xhrGetData).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(xhrGetData[k])
        }).join('&');

        //Open Request
        oReq.open(
            xhrGetForm.getAttribute('method').toUpperCase(),
            xhrGetForm.getAttribute('action') + '?' + urlParams,
            true);

        //Send request - no payload needed as it's all part of the GET URL, where we've converted all object keys and values to query parameters and values
        oReq.send();
    });

    //Fetch API - POST
    fetchPostForm.addEventListener('submit', function(e){
        //Get form data upon form submit
        var fetchPostData = apiObjectGen('Fetch','POST');
        console.log('Attempting to use Fetch API for sending data to server using POST method, data:', fetchPostData);

        //Prevent browser's native form submit as we'll use own solution
        e.preventDefault();

        //Prepare new Request
        var oReq = new Request(fetchPostForm.getAttribute('action'), {method: fetchPostForm.getAttribute('method').toUpperCase(), body: JSON.stringify(fetchPostData)});

        fetch(oReq)
        .then(function(response) {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(function(response) {
            console.log('Response from server:', response);
        }).catch(function(error) {
            console.error(error);
        });
    });

    //Fetch API - GET
    fetchGetForm.addEventListener('submit', function(e){
        var fetchGetData = apiObjectGen('Fetch','GET');
        console.log('Attempting to use Fetch API for sending data to server using GET method, data:', fetchGetData);
        
        //Prevent browser's native form submit as we'll use own solution
        e.preventDefault();

        //Prepare JSON data as GET parameters
        var urlParams = Object.keys(fetchGetData).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(fetchGetData[k])
        }).join('&');

        //Prepare new Request
        var oReq = new Request(fetchGetForm.getAttribute('action') + '?' + urlParams, {method: fetchGetForm.getAttribute('method').toUpperCase()});

        fetch(oReq)
        .then(function(response) {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(function(response) {
            console.log('Response from server:', response);
        }).catch(function(error) {
            console.error(error);
        });
    });
})