/* This will look similar to the server.js code, but it's only NodeJS + fastify 
* purpose is to serve/host html, css, and JS code - flat from the client/public 
* folder
*/

const fastify = require('fastify')({ logger: true });
const serverPort = 80;
const path = require('path')
const staticAssetsPath = '/assets/'; //for all css, images etc assets to be served inside views/rendered html
const staticLocalFolder = path.join(__dirname, '/client/public'); //local folder mapped to the assets path

//Static files for Fastify - css, images, non-NodeJS scripts
fastify.register(require('fastify-static'), {
    root: staticLocalFolder,
    prefix: staticAssetsPath,
});

//EJS Template Renderer for Fastify
fastify.register(require('point-of-view'), {
    engine: {
        ejs: require('ejs') //we'll map ejs but not use it inside the html files, as we aim to use flat HTML in the client.js 
    }
});

// Home root route
fastify.get('/', async (request, reply) => {
    reply.view(`./client/public/index.html`);
    return reply;
});

// Run the server!
const start = async () => {
    try {
        await fastify.listen(serverPort);
        fastify.log.info(`CLIENT static server listening on ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
}

start();